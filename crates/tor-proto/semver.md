BREAKING (experimental): Remove HsNtorServiceInput.
BREAKING (experimental): Remove IncomingStream::is_rejected().
BREAKING (experimental): API change on IncomingStream::discard().
